import config
import thread
import post
import board
import channel
import gobject
import time
import datetime
import urlparse
import imp
import os 

class Daemon:
    def __init__(self,confPath):
        self.path = os.curdir
        if not os.path.exists(os.path.join(self.path, 'boards')) and os.path.exists(os.path.join(self.path, 'daemon')):
            self.path = os.path.join(self.path, 'daemon')
        self.boardsPath = os.path.join(self.path, 'boards')
        self.configPath = confPath
        self.options = config.read_config(self.configPath)
        print self.options
        self.boardEngines = {}
        for plugin in os.listdir(self.boardsPath):
            if plugin.startswith('board_') and plugin.endswith('.py'):
                print plugin
                plugin = plugin[:-3]
                board_type = plugin[6:]
                file, pathname, description = imp.find_module(plugin, [self.boardsPath])
                metaboard = imp.load_module(plugin, file, pathname, description).MetaBoard
                self.boardEngines[board_type] = metaboard
        print self.boardEngines
        self.ChansPool = {}
        self.ThreadPool = {}
        for name in self.options['chans']:
            self.ChansPool[name] = channel.Channel(name, self.options['chans'][name], self.boardEngines)
        print self.ChansPool
        
    def addThread(self,thread):
        if not thread.URI in self.ThreadPool:
            self.ThreadPool[thread.URI] = thread
            return thread
        elif self.ThreadPool[thread.URI] == thread:
            return thread
        else:
            #Something is horribly wrong here, but we will just re-map to new thread for now
            self.ThreadPool[thread.URI] = thread
            return thread
            
    def getThreadByURL(self,url):
        print url
        urlp = urlparse.urlparse(url)
        for c in self.ChansPool:
            chan = self.ChansPool[c]
            if urlp.hostname == chan.host:
                print "Matches %s" % chan
                for b in chan.BoardsPool:
                    if urlp.path.find(chan.BoardsPool[b].ruri) == 0:
                        return self.addThread(chan.BoardsPool[b].getThreadByURL(url))
                break
        return None