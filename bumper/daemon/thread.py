import httputils

class Thread:

    def __init__(self, board=None, id=None, directUrl=None):
        self.board = board
        self.id = id
        self.directUrl = directUrl
        self.posts = []
        # Actually we should process urls with according plugins, but will do for now
        if not self.id and self.directUrl:
            self.id = int(directUrl.split("/")[-1].split(".")[0])
        if not self.directUrl and self.id and self.board:
            self.directUrl = self.board.chan.proto + u"://" + self.board.chan.host + u":" + self.board.chan.port + self.board.ruri + u"res/" + str(self.id) + (self.board.extension and (u"." + self.board.extension) or u"")
        self.URI  = "%s/%s" % (self.board.URI, str(self.id))
        self.exists = True
        self.lastPostedDate = None
        self.postCount = 0
        self.lastPostId = None
        
    def __repr__(self):
        return "<Thread %s (%s, %s)>" % (self.URI, self.exists and "Alive" or "Dead", self.directUrl)   
   
    def show(self):
        pass

    def hide(self):
        pass

    def watch(self):
        pass

    def unwatch(self):
        pass
    
    def status(self):
        stats = httputils.HTTPHead(self.directUrl)
        if stats[0] == 200:
            self.exists = True
            self.lastPostedDate = stats[1]
        else:
            self.exists = False
        return stats