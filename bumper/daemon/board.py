class Board:
    def __init__(self, chan, name, type):
        self.name = name
        self.ruri = u"/%s/" % name
        self.type = type
        self.chan = chan
        self.extension = 'html'
        self.capabilities = None
        self.fieldsMapping = None
        self.URI  = "%s/%s" % (self.chan.URI, self.name)
        self.ThreadsPool = {}
        
    def __repr__(self):
        return "<Board %s (%s)>" % (self.URI, self.type)
    
    def addThread(self,thread):
        if not thread.URI in self.ThreadsPool:
            self.ThreadsPool[thread.URI] = thread
            return thread
        else:
            return self.ThreadsPool[thread.URI]
            