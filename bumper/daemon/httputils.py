# coding=utf-8
import sys
import urllib2
import httplib
import os
import cgi
import shutil
import datetime
import time
import urlparse
import re 

monthes = [('Янв','Jan','января'),('Фев','Feb','февраля'),('Мар','Mar','марта'),('Апр','Apr','апреля'),('Май','May','мая'),('Июн','Jun','июня'),('Июл','Jul','июля'),('Авг','Aug','августа'),('Сен','Sep','сентября'),('Окт','Oct','октября'),('Ноя','Nov','ноября'),('Дек','Dec','декабря')]
dateRe = re.compile(r"""[^\d]+(\d+)\s+([^\d\s]+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)""")
dateReISO = re.compile(r"""(\d+)\-(\d+)\-(\d+) (\d+)\:(\d+)\:(\d+)""")
def getDateTime(date):
    dateP  = dateRe.findall(date)
    dateP  = dateP[0]
    mi = 0
    f  = False
    for mm in monthes:
        mi = mi + 1
        if dateP[1] in mm:
            f = True
            break
    if f:
        return datetime.datetime(int(dateP[2]),mi,int(dateP[0]),int(dateP[3]),int(dateP[4]),int(dateP[5]))
    else:
        return None
def getDateTimeFromISO8601(date):
    dateP = dateReISO.findall(date)
    dateP  = dateP[0]
    return datetime.datetime(int(dateP[0]),int(dateP[1]),int(dateP[2]),int(dateP[3]),int(dateP[4]),int(dateP[5]))



def HTTPHead(link):
    linkp = urlparse.urlparse(link)
    c = httplib.HTTPConnection(linkp.hostname, linkp.port and int(linkp.port) or 80)
    c.request('HEAD', linkp.path)
    r = c.getresponse() 
    if r.status == 200:
        size = r.getheader('content-length',0)
        date = r.getheader('last-modified',r.getheader('date',None))
        return (200, getDateTime(date), int(size))
    else:
        return (r.status,None,None)
    
