import os
import gtk
import gtk.glade
import gobject
import time
import datetime
import daemon
import random

TI = None
PW = None
SW = None

class GladeWindow:
    def __init__(self, gladefile, window):
        self.gladefile = gladefile
        self.windowId = window
        self.ui = None
        self.window = None
        
    def show(self):
        if not (self.window):
            self.ui = gtk.glade.XML(self.gladefile)
            self.window = self.ui.get_widget(self.windowId)
            self.window.connect("delete_event", self.delete_event)
            self.window.show()
            return True
        else:
            return False
    
    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]
        else:
            return self.ui.get_widget(name)
            
    def destroy(self):
        self.window.destroy()
        self.window = None
        self.ui = None
        
    def delete_event(self, widget, event, data=None):
        self.destroy()
            
class StatusWindow(GladeWindow):
    def __init__(self):
        GladeWindow.__init__(self,"glade/bumper_status.glade","StatusWindow")
    def show(self):
        if GladeWindow.show(self):
            pass
            
class CaptchaWindow(GladeWindow):
    def __init__(self):
        GladeWindow.__init__(self,"glade/bumper_captcha.glade","CaptchaWindow")
        
    def show(self, img):
        if GladeWindow.show(self):
            self.window.stick()
            #self.window.set_decorated(False)
            self.CaptchaImage.set_from_file(img)
            self.Captcha.connect("activate", self.on_captcha, self)
            
    def on_captcha(self, widget, event, data=None):
        captcha = self.Captcha.get_text()
        self.destroy()
        bumper.doBump(captcha)
        
class PreferencesWindow(GladeWindow):
    def __init__(self):
        GladeWindow.__init__(self,"glade/bumper_preferences.glade","PreferencesWindow")

    def show(self):
        if GladeWindow.show(self):
            self.IntervalField.set_text(str(bumper.timerInterval))
            self.postTitle.set_text(bumper.postTitle)
            self.postMessage.set_text(bumper.postMessage)
            self.postPassword.set_text(bumper.postPassword)
            self.OkButton.connect("clicked", self.on_ok, self)
            self.StatusButton.connect("clicked", self.on_status, self)
            self.AddThreadsButton.connect("clicked", self.on_addThreads, self)
            
            
    def on_ok(self, widget, event, data=None):
        bumper.timerInterval = int(self.IntervalField.get_text())
        bumper.postTitle = self.postTitle.get_text()
        bumper.postMessage = self.postMessage.get_text()
        #bumper.postImages = None
        bumper.postPassword = self.postPassword.get_text()
        
    def on_status(self, widget, event, data=None):
        SW.show()

    def on_addThreads(self, widget, event, data=None):
        inBuffer = self.ThreadsField.get_buffer()
        outBuffer = self.OutputField.get_buffer()
        urlList = inBuffer.get_text(inBuffer.get_start_iter(),inBuffer.get_end_iter()).split("\n")
        f = False
        for url in urlList:
            outBuffer.insert(outBuffer.get_end_iter(), "Url %s ... " % url)
            thread = bumper.addThreadByUrl(url)
            gtk.main_iteration(False)
            if thread:
                outBuffer.insert(outBuffer.get_end_iter(), str(thread))
                f = True
            else:
                outBuffer.insert(outBuffer.get_end_iter(), "Failed!")
            outBuffer.insert(outBuffer.get_end_iter(), "\r\n")
        if f:
            bumper.setBumping(True)

class BumperStatusIcon(gtk.StatusIcon):
    def __init__(self):
        gtk.StatusIcon.__init__(self)
        menu = '''
            <ui>
             <menubar name="Menubar">
              <menu action="Menu">
               <menuitem action="Notify"/>
               <menuitem action="Status"/>
               <separator/>
               <menuitem action="Preferences"/>
               <menuitem action="About"/>
               <menuitem action="Quit"/>
              </menu>
             </menubar>
            </ui>
        '''
        actions = [
            ('Menu',  None, 'Menu'),
            ('Preferences', gtk.STOCK_PREFERENCES, '_Preferences...', None, 'Change Bumper preferences', self.on_preferences),
            ('Notify', gtk.STOCK_APPLY, '_Notify...', None, 'Notify', self.on_activate),
            ('Status', gtk.STOCK_INFO, '_Status...', None, 'Status', self.on_status),
            ('About', gtk.STOCK_ABOUT, '_About...', None, 'About Bumper', self.on_about),
            ('Quit', gtk.STOCK_QUIT, '_Quit...', None, 'Quit', self.on_quit)]
        ag = gtk.ActionGroup('Actions')
        ag.add_actions(actions)
        self.manager = gtk.UIManager()
        self.manager.insert_action_group(ag, 0)
        self.manager.add_ui_from_string(menu)
        self.menu = self.manager.get_widget('/Menubar/Menu/About').props.parent
        self.set_from_file(IconFile)
        self.set_tooltip("PyChans Thead Bumper")
        self.set_visible(True)
        self.set_blinking(False)
        self.connect('activate', self.on_activate)
        self.connect('popup-menu', self.on_popup_menu)

    def on_activate(self, data):
        bumper.toggleBumping()

    def on_popup_menu(self, status, button, time):
        self.menu.popup(None, None, None, button, time)

    def on_preferences(self, data):
        PW.show()
        
    def on_status(self, data):
        SW.show()
        
    def on_quit(self,data):
        self.set_visible(False)
        gtk.main_quit()
        
    def on_about(self, data):
        dialog = gtk.AboutDialog()
        dialog.set_name('Bumper')
        dialog.set_version('0.0.0')
        dialog.set_comments('Threads tracking and bumping tool')
        dialog.set_website('2ch.ru')
        dialog.run()
        dialog.destroy()

class Bumper:
    def __init__(self):
        self.threads = {}
        self.boardThreads = {}
        self.boards = {}
        self.pendingBumps = []
        self.timerInterval = 30*1000
        self.doBumps = False
        self.currentPost = None
        self.postTitle = "YAD defender"
        self.postMessage = "[YAD defender][$RND$]"
        self.postImages = None
        self.postPassword = "HolyBump"
        self.timerHandle = gobject.timeout_add(self.timerInterval, self.timer)
        
    def toggleBumping(self):
        self.setBumping(not self.doBumps)
        
    def setBumping(self, s):
        self.doBumps = s
        TI.set_blinking(s)
    
    def reAddTimer(self):
        self.timerHandle = gobject.timeout_add(self.timerInterval, self.timer)
        
    def timer(self):
        self.reAddTimer()
        print "%s timer(), i=%s" % (datetime.datetime.now(),self.timerInterval)
        print self.threads
        print self.boardThreads
        for b in self.boardThreads:
            threads = self.boards[b].loadPages([0])
            gtk.main_iteration(False)
            for t in list(self.boardThreads[b]):
                if t in threads:
                    print "Thread %s is in list" % t
                else:
                    print "Thread %s is not in list" % t
                    thread = self.threads[t]
                    thread.status()
                    gtk.main_iteration(False)
                    if thread.exists:
                        if not thread in self.pendingBumps:
                            self.pendingBumps.append(thread)
                            print "Will bump it"
                    else:
                        print "Its dead!"
                        del self.boardThreads[b][t]
                        if thread in self.pendingBumps:
                            self.pendingBumps.remove(thread)
        for b in self.boardThreads:
            if not len(self.boardThreads[b]):
                del self.boardThreads[b]
        print self.pendingBumps
        self.processBumps()
        
    def processBumps(self):
        if self.doBumps and self.pendingBumps and not self.currentPost:
            self.initBump(self.pendingBumps[0])
                
    def initBump(self,thread):
        self.currentPost = thread.newPost()
        if 'captcha' in self.currentPost.fields:
            captchaImage = self.currentPost.loadCaptcha()
            print captchaImage
            CW.show(captchaImage)
        else:
            self.doBump()
            
    def doBump(self, captcha=None):
        gtk.main_iteration(False)
        if 'text' in self.currentPost.fields:
            self.currentPost.fields['text'] = self.postMessage.replace("$RND$", str(random.randint(1,100000)))
        if 'password' in self.currentPost.fields:
            self.currentPost.fields['password'] = self.postPassword
        if 'title' in self.currentPost.fields:
            self.currentPost.fields['title'] = self.postTitle
        if captcha and 'captcha' in self.currentPost.fields:
            self.currentPost.fields['captcha'] = captcha
        
        r = self.currentPost.post()
        if self.currentPost.thread in self.pendingBumps:
            self.pendingBumps.remove(self.currentPost.thread)
        self.currentPost = None
        self.processBumps()
        
    def addThreadByUrl(self,url):
        thread = Daemon.getThreadByURL(url)
        if thread and thread.status()[0] == 200:
            self.threads[thread.URI] = thread
            if not thread.board.URI in self.boards:
                self.boards[thread.board.URI] = thread.board
            if not thread.board.URI in self.boardThreads:
                self.boardThreads[thread.board.URI] = {}
            self.boardThreads[thread.board.URI][thread.URI] = thread
            return thread
        elif thread:
            print "Thread %s is dead" % thread
            return thread
        else:
            return None
        

if __name__ == '__main__':
    IconFile = "icons/2ch.png"
    gtk.window_set_default_icon_from_file(IconFile)
    TI = BumperStatusIcon()
    PW = PreferencesWindow()
    SW = StatusWindow()
    CW = CaptchaWindow()
    Daemon = daemon.Daemon('config.cfg')
    bumper = Bumper()
    PW.show()
    gtk.main()