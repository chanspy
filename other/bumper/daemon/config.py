import ConfigParser

def read_config(path):
    options = {}

    config = ConfigParser.ConfigParser()
    config.readfp(open(path))
    options = {'chans':{}}
    chans = options['chans']
    for chan in config.sections():
        chans[chan] = {}
        for o in config.items(chan):
            chans[chan][o[0]] = o[1]
            
        chans[chan]['boards'] = {}
        for boards_by_type in config.get(chan, 'boards').split(';'):
            board_type, boards = boards_by_type.split(':')
            chans[chan]['boards'][board_type] = boards.split(',')

    return options
