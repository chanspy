try:
    from board import Board
    from thread import Thread
except ImportError:
    from daemon.board import Board
    from daemon.thread import Thread
    
import urlparse
import urllib2
import urllib
import httplib
from lxml import etree
import StringIO
import re
import time
import Image
import os

replyIdRe = re.compile(r""">>(\d+)""")
postIdRe = re.compile(r"""\/(\d+)\.x?h?t?ml?(#i?(\d+))?""")

class MetaBoard(Board):
    def __init__(self, chan, name, type):
        Board.__init__(self, chan, name, type)
        self.baseURL = self.chan.proto + u"://" + self.chan.host + u":" + self.chan.port + self.ruri
        
    def __repr__(self):
        return "<WakabaBoard %s (%s)>" % (self.URI, self.type)
        
    def getThreadByURI(self, URI, id):
        if URI in self.ThreadsPool:
            return self.ThreadsPool[URI]
        else:
            return self.addThread(WakabaThread(board=self, id=id))
            
    def getThreadById(self, id):
        return self.getThreadByURI("%s/%s" % (self.URI,str(id)), id)
            
    def getThreadByURL(self, url):
        return self.getThreadById(int(url.split("/")[-1].split(".")[0]))
            
    def loadPages(self, pages):
        r = {}
        for page in pages:
            p = self.loadPage(page)
            for t in p:
                r[t] = p[t]
        return r
        
    def loadPage(self, page):
        url = self.baseURL
        r = {}
        if page:
            url += str(page) + u"." + self.extension
        html = urllib2.urlopen(url).read()
        posts = self.initialParsing(html)
        for p in posts:
            if p.threadId == p.postId:
                t = self.getThreadById(p.threadId)
                r[t.URI] = t
        return r
    
    def initialParsing(self, html):
        parser = etree.HTMLParser()
        if isinstance(html, str):
            try:
                html = html.decode('utf-8')
            except UnicodeDecodeError:
                pass
        document = etree.parse(StringIO.StringIO(html), parser)
        if not self.capabilities:
            self.getCapabilities(document)
        posts = document.xpath("/html/body/form//*[@class='reflink']/a")
        result = []
        for a in posts:
            result.append(ParsedPost(a))
        return result
        
    def getCapabilities(self, document):
        c = {}
        f = {}
        fields = ['name','email','title']
        textfields = []
        form = document.xpath("/html/body//form")[0]
        inputs = form.xpath("input") + form.xpath("*//input")
        textarea = form.xpath("*//textarea")[0]
        imgs = form.xpath("*//img")
        action = form.get("action")
        if action[0] == '/':
            action = self.chan.proto + u"://" + self.chan.host + u":" + self.chan.port + action
        c['action'] = action
        c['static'] = {}
        
        for img in imgs:
            src = img.get("src")
            if src and src.lower().find('captcha'):
                captcha = src.split("?")[0]
                if captcha[0] == '/':
                    captcha = self.chan.proto + u"://" + self.chan.host + u":" + self.chan.port + captcha
                cfield = img.getparent().getparent().xpath("*//input")[0]
                c['captcha'] = captcha
                f['captcha'] = cfield.get("name")
                if cfield in inputs:
                    inputs.remove(cfield)
                break
        f['text'] = textarea.get("name")
        for field in inputs:
            t = field.get("type","text").lower()
            n = field.get("name")
            if t == "password":
                f['password'] = n
            elif t == "file":
                c['file'] = True
                f['file'] = n
            elif t == "hidden":
                if n == "parent":
                    f['parent'] = n
                else:
                    c['static'][n] = field.get("value")
            elif t == "text":
                textfields.append(field)
            elif t == "radio":
                f['gb2'] = n
            else:
                c['static'][n] = field.get("value")
        for i in range(len(textfields)):
            field = textfields.pop()
            if fields:
                ftype = fields.pop()
                f[ftype] = field.get('name')
            else:
                c['static'][field.get('name')] = field.get("value")
        if not 'parent' in f:
            f['parent'] = 'parent'
        self.capabilities = c
        self.fieldsMapping = f
        print self.capabilities
        print self.fieldsMapping
        

class ParsedPost:
    def __init__(self, a, tid=None):
        self.a = a
        self.href = a.get('href')
        if tid:
            ids = replyIdRe.findall(self.href)
            self.threadId = tid
            self.postId = int(ids[0])
        else:
            ids = postIdRe.findall(self.href)
            self.threadId = int(ids[0][0])
            self.postId = ids[0][2] and int(ids[0][2]) or int(ids[0][0])
        self.reflink = a.getparent()

class WakabaNewPost:
    def __init__(self, thread):
        self.thread = thread
        if not self.thread.board.capabilities:
            self.thread.board.loadPage(0)
        self.capabilities = self.thread.board.capabilities
        self.fieldsMapping = self.thread.board.fieldsMapping            
        self.fields = {}
        for f in self.fieldsMapping:
            self.fields[f] = None
        
        
        
    def loadCaptcha(self):
        if self.capabilities.get("captcha",None):
            url = self.capabilities.get("captcha") + u"?key=res%s&dummy=" % self.thread.id
            try:
                u = urllib2.urlopen(url)
                if u.headers.maintype == "image":
                    img = u.read()
                    name = "tmp/%s.%s" % (self.thread.id, u.headers.subtype)
                    fl = open(name,"w+b")
                    fl.write(img)
                    fl.close()
                    fl = None
                    if u.headers.subtype != "png":
                        img = Image.open(name)
                        name2 = "tmp/%s.%s" % (self.thread.id, "png")
                        img.save(name2)
                        img = None
                        os.unlink(name)
                        return name2
                    return name
                else:
                    return None
            except urllib2.HTTPError, e:
                print "Failed to load captcha, %s" % e
                return None
        else:
            return None
    def post(self):
        fields = []
        for f in self.capabilities['static']:
            if f:
                fields.append((f, self.capabilities['static'][f] or u""))
        self.fields['parent'] = str(self.thread.id)
        for f in self.fields:
            if f:
                fields.append((self.fieldsMapping[f], self.fields[f] or u""))
        print fields
        data = urllib.urlencode(fields)
        print data
        try:
            return self.checkResult(urllib2.urlopen(self.capabilities['action'],data))
        except urllib2.HTTPError, e:
            print "Failed to post, %s" % e
            return False
    def checkResult(self, req):
        pg = req.read()
        print req.headers
        print pg
        return False
        
class WakabaThread(Thread):
    def __repr__(self):
        return "<WakabaThread %s (%s, %s)>" % (self.URI, self.exists and "Alive" or "Dead", self.directUrl)      
        
    def newPost(self):
        return WakabaNewPost(self)