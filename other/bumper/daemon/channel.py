class Channel:
    def __init__(self, chan, definition, engines):
        self.name = chan
        self.host = definition.get("host",chan)
        self.port = definition.get("port","80")
        self.proto= definition.get("proto","http")
        self.URI  = "/%s" % self.name
        self.BoardsPool = {}
        boards = definition.get("boards",{})
        for btype in boards:
            for bname in boards[btype]:
                b = engines[btype](chan=self,name=bname,type=btype)
                self.BoardsPool[b.URI] = b
        
        
    def __repr__(self):
        return "<Channel %s (%s %s %s)>" % (self.URI, self.proto, self.host, self.port)