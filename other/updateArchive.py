# coding=utf-8
import os
import cgi
import shutil
import datetime
import time
import Image
import hashlib
import re
import sys
import urllib2
import httplib
from lxml import etree
import StringIO
import logging

def can_import(name):
    """Attempt to __import__ the specified package/module, returning True when
    succeeding, otherwise False"""
    try:
        __import__(name)
        return True
    except ImportError:
        return False

def unicodify(text):
    if isinstance(text, str):
        text = text.decode('utf-8')
    return text

idList = {}
GFilters = {}

class DateTimeParser:
    monthes = [('Янв','Jan','января'),('Фев','Feb','февраля'),('Мар','Mar','марта'),('Апр','Apr','апреля'),('Май','May','мая'),('Июн','Jun','июня'),('Июл','Jul','июля'),('Авг','Aug','августа'),('Сен','Sep','сентября'),('Окт','Oct','октября'),('Ноя','Nov','ноября'),('Дек','Dec','декабря')]
    dateRe = re.compile(r"""[^\d]+(\d+)\s+([^\d\s]+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)""")
    dateReISO = re.compile(r"""(\d+)\-(\d+)\-(\d+) (\d+)\:(\d+)\:(\d+)""")
    def getDateTime(self,date):
        dateP  = self.dateRe.findall(date)
        dateP  = dateP[0]
        mi = 0
        f  = False
        for mm in self.monthes:
            mi = mi + 1
            if dateP[1] in mm:
                f = True
                break
        if f:
            return datetime.datetime(int(dateP[2]),mi,int(dateP[0]),int(dateP[3]),int(dateP[4]),int(dateP[5]))
        else:
            return None
    def getDateTimeFromISO8601(self,date):
        dateP = self.dateReISO.findall(date)
        dateP  = dateP[0]
        return datetime.datetime(int(dateP[0]),int(dateP[1]),int(dateP[2]),int(dateP[3]),int(dateP[4]),int(dateP[5]))
    
DTP = DateTimeParser()

class IBParser:
    def GetNextTag(self,el,tag,skip=0):
        tag = tag.lower()
        if skip:
            r = el.getnext()
        else:
            r = el
        if not r.tag or r.tag.lower() != tag:
            while (r.getnext() != None) and not (r.getnext().tag and r.getnext().tag.lower() == tag):
                r = r.getnext()
            if r.getnext() != None:
                r = r.getnext()
        if r.tag and r.tag.lower() == tag:
            return r
        else:
            return None
    
    def GetPreviousTag(self,el,tag,skip=0):
        tag = tag.lower()
        if skip:
            r = el.getprevious()
        else:
            r = el
        if not r.tag or r.tag.lower() != tag:
            while (r.getprevious() != None) and not (r.getprevious().tag and r.getprevious().tag.lower() == tag):
                r = r.getprevious()
            if r.getprevious() != None:
                r = r.getprevious()
        if r.tag and r.tag.lower() == tag:
            return r
        else:
            return None
    def GetPostID(self,post):
        if post.thread:
            ids = self.replyIdRe.findall(post.href)
            return [post.thread.tid,int(ids[0])]
        else:
            ids = self.postIdRe.findall(post.href)
            return [int(ids[0][0]),ids[0][2] and int(ids[0][2]) or int(ids[0][0])]
            
class Loader:
    def parseLink(self,link):
        s1 = link.split('://')
        p = len(s1)>1 and s1[0] or None
        p2= p and (p+'://') or ''
        s2 = s1[-1].split('/')
        return [p, s2[0], p2 + s2[0] + '/', p2 + '/'.join(s2[:-1]) + '/', s2[-1],'/'+'/'.join(s2[1:])]
        
class LoaderLocal(Loader):
    def __init__(self,link):
        p = self.parseLink(link)
        self.relativeUrl = p[3]
    def stat(self,link):
        try:
            stats = os.stat(link)
            return [datetime.datetime.fromtimestamp(stats[8]),stats[6]]
        except OSError:
            return None
    def get(self,url):
        return open(url,'rb').read()
    def getAbsolutePath(self,url):
        return self.relativeUrl + url
    def getFromRelative(self,url):
        return self.get(self.getAbsolutePath(url))
                            
class LoaderHTTP(Loader):
    def __init__(self,link):
        p = self.parseLink(link)
        self.proto = p[0]
        self.host = p[1]
        self.baseUrl = p[2]
        self.relativeUrl = p[3]
    def stat(self,link):
        linkp = self.parseLink(link)
        c = httplib.HTTPConnection(linkp[1])
        c.request('HEAD', linkp[5])
        r = c.getresponse() 
        if r.status == 200:
            size = r.getheader('content-length',0)
            date = r.getheader('last-modified',r.getheader('date',None))
            return [DTP.getDateTime(date),size]
        elif r.status == 404:
            return None
        else:
            return None
    def get(self,url):
        req = urllib2.Request(url)
        req.add_header('Referer', self.baseUrl)
        try:
            f = urllib2.urlopen(req)
            res = f.read()
            return res
        except urllib2.HTTPError:
            return None
    def getAbsolutePath(self,url):
        if url[0] == '/':
            return self.baseUrl + url
        else:
            return self.relativeUrl + url
    def getFromRelative(self,url):
        return self.get(self.getAbsolutePath(url))

class IBFilter:
    def filter(self,post):
        return None
class IBFilterSage(IBFilter):
    def filter(self,post):
        return post.sage
class IBFilterLowres(IBFilter):
    def filter(self,post):
        return post.pic and post.pic.width < 50
        
class Thread:
    def __init__(self,entry,parsers,directlink=None,forcetype=None):
        self.parser = parsers[entry.type]
        self.tid    = entry.tid
        self.url    = entry.url
        self.board  = entry.board
        self.chanTag= entry.chanTag
        self.tags   = entry.tags and entry.tags.split(',') or []
        self.type   = entry.type
        self.forcetype = forcetype
        self.lastChanged =  entry.lastChanged
        self.filters = []
        filters = entry.filters and entry.filters.split(',') or []
        if filters:
            for f in filters:
                self.filters.append(GFilters[f])
        
        self.timeDiff = entry.timeDiff
        self.directlink = directlink
        self.loader = Loader()
        if not self.directlink:
            self.directlink = self.parser.GetThreadLink(self.url,self.board,self.tid)
        if self.loader.parseLink(self.directlink)[0]:
            self.loader = LoaderHTTP(self.directlink)
        else:
            self.loader = LoaderLocal(self.directlink)
    def checkState(self):
        stat = self.loader.stat(self.directlink)
        if not stat:
            return [404]
        elif stat[0] > self.lastChanged:
            return [200,stat[0],stat[1]]
        else:
            return [304,stat[0],stat[1]]
            
    def initialize(self):
        page = self.loader.get(self.directlink)
        if page:
            parser = etree.HTMLParser()
            if isinstance(page, str):
                page = page.decode('utf-8')
            self.document = etree.parse(StringIO.StringIO(page), parser)
            self.posts = self.parser.GetPostsList(self)
            if self.posts:
                return True
            else:
                return False
        else:
            return False
    def filter(self,post):
        fl = None
        if self.filters:
            for f in self.filters:
                fl = fl or f.filter(post)
        return fl
        
class WakabaParser(IBParser):
    replyIdRe = re.compile(r""">>(\d+)""")
    postIdRe = re.compile(r"""\/(\d+)\.x?h?t?ml?(#i?(\d+))?""")
    def GetThreadLink(self,url,board,thread):
        return 'http://'+url+'/'+board+'/res/'+str(thread)+'.html'
    def GetPostsList(self,thread):
        posts = thread.document.xpath("/html/body/form//*[@class='reflink']/a")
        postsList = []
        if posts:
            for postA in posts:
                post = Post()
                post.thread = thread
                post.href = postA.get('href')
                post.reflink = postA.getparent()
                post.Ids = self.GetPostID(post)
                postsList.append(post)
            return postsList
        else:
            return None
            
    def GetImgSrc(self,post):
        cont = post.l.getparent()
        for t in cont:
            if t.tag.lower() == 'a':
                href = t.get('href')
                if href and href.find('/src/') != -1:
                    if post.thread.forcetype:
                        return '../src/' + post.thread.loader.parseLink(href)[4]
                    else:
                        return href
        return None
    
    def ParseText(self,post):
        if post.bq is not None:
            post.bq.tail = ''
            message = etree.tostring(post.bq, pretty_print=False,encoding='utf-8')
            if message[:12].lower() == '<blockquote>' and message[-13:].lower() == '</blockquote>':
                message = message[12:-13]
            else:
                print "Cant parse this message : '%s'" % message
                return None
            return message
        else:
            return u''
    def parsePost(self,post):
        post.bq = self.GetNextTag(post.reflink,'blockquote')
        post.l  = self.GetPreviousTag(post.reflink,'label')
        post.title = unicodify(post.l[1].text)
        if not post.title:
            post.title = u''
        post.cpn = post.l[2]
        post.sage = False
        if len(post.cpn)>0 and post.cpn[0].tag.lower() == 'a':
            post.cpnHref = post.cpn[0].get('href')
            if post.cpnHref.find('sage') > -1:
                post.sage = True	
        post.src = self.GetImgSrc(post)
        date = post.l[2].tail.encode('utf-8')
        date = date.replace("\r",'').replace("\n",'')
        post.date = DTP.getDateTime(date)
        post.message = unicodify(self.ParseText(post))

class UpdateArchive(Command):
    # Parser configuration
    summary = "--NO SUMMARY--"
    usage = "--NO USAGE--"
    group_name = "fc"
    parser = Command.standard_parser(verbose=False)
    parser.add_option("--mode")
    parser.add_option("--chan")
    parser.add_option("--board")
    parser.add_option("--thread")
    parser.add_option("--chanTag")
    parser.add_option("--type")
    parser.add_option("--tags")
    parser.add_option("--timeDiff")
    parser.add_option("--directlink")
    parser.add_option("--list")
    parser.add_option("--filters")
    parser.add_option("--forcetype")
    parsers = {'wakaba':WakabaParser()}
    def command(self):
        """Main command to create a new shell"""
        self.verbose = 3
        config_file = 'development.ini'
        config_name = 'config:%s' % config_file
        here_dir = os.getcwd()
        locs = dict(__name__="pylons-admin")
        conf = appconfig(config_name, relative_to=here_dir)
        conf.update(dict(app_conf=conf.local_conf,global_conf=conf.global_conf))
        paste.deploy.config.CONFIG.push_thread_config(conf)
        sys.path.insert(0, here_dir)
        wsgiapp = loadapp(config_name, relative_to=here_dir)
        test_app = paste.fixture.TestApp(wsgiapp)
        tresponse = test_app.get('/_test_vars')
        request_id = int(tresponse.body)
        test_app.pre_request_hook = lambda self:paste.registry.restorer.restoration_end()
        test_app.post_request_hook = lambda self:paste.registry.restorer.restoration_begin(request_id)
        paste.registry.restorer.restoration_begin(request_id)
        egg_info = find_egg_info_dir(here_dir)
        f = open(os.path.join(egg_info, 'top_level.txt'))
        packages = [l.strip() for l in f.readlines() if l.strip() and not l.strip().startswith('#')]
        f.close()
        found_base = False
        for pkg_name in packages:
            # Import all objects from the base module
            base_module = pkg_name + '.lib.base'
            found_base = can_import(base_module)
            if not found_base:
                # Minimal template
                base_module = pkg_name + '.controllers'
                found_base = can_import(base_module)

            if found_base:
                break

        if not found_base:
            raise ImportError("Could not import base module. Are you sure this is a Pylons app?")

        base = sys.modules[base_module]
        base_public = [__name for __name in dir(base) if not \
                       __name.startswith('_') or __name == '_']
        for name in base_public:
            locs[name] = getattr(base, name)
        locs.update(dict(wsgiapp=wsgiapp, app=test_app))

        mapper = tresponse.config.get('routes.map')
        if mapper:
            locs['mapper'] = mapper
            
            
        self.thread = self.options.thread
        self.chan = self.options.chan
        self.chanTag = self.options.chanTag
        self.board = self.options.board
        
        logging.getLogger('sqlalchemy').setLevel(logging.ERROR)
        GFilters['sage'] = IBFilterSage()
        GFilters['lowres'] = IBFilterLowres()
        #logging.getLogger( 'sqlalchemy').setLevel( logging.NONE )
        if not self.options.mode or self.options.mode == 'update':
            self.UpdateArchive()
        elif self.options.mode == 'add':
            self.AddToArchive()
        elif self.options.mode == 'thread':
            if self.options.list:
                f = open(self.options.list,'r')
                tList = f.readlines()
            else:
                tList = [self.options.thread]
            for t in tList:
                entry = ArchiveList()
                entry.tid = int(t)
                entry.url = self.options.chan
                entry.chanTag = self.options.chanTag
                entry.board = self.options.board or 'b'
                entry.tags = self.options.tags or ''
                entry.type = self.options.type or 'wakaba'
                entry.filters = self.options.filters or ''
                entry.timeDiff = self.options.timeDiff or 0
                entry.lastChanged = datetime.datetime.fromtimestamp(0)
                print "Processing %s %s %s %s" % (entry.tid,entry.url,entry.chanTag,entry.board)
                thread = Thread(entry,self.parsers,self.options.directlink,self.options.forcetype)
                self.processThread(thread)

    def LoadPage(self,thread,chan='2ch.ru',board='b'):
        self.host = 'http://'+chan
        if thread:
            self.path = '/'+board+'/res/'
            self.url = self.host+self.path+thread+'.html'
        else:
            self.path = '/'+board+'/'
            self.url = self.host+self.path
        print self.url
        req = urllib2.Request(self.url)
        req.add_header('Referer', self.host+'/'+board+'/')
        f = urllib2.urlopen(req)
        res = f.read()
        return res
        
    def getTags(self,tagsList):
        tags = []
        for tagName in tagsList:
            tag = meta.Session.query(Tag).filter(Tag.tag==tagName).first()
            if tag:
                tags.append(tag)
            else:
                tags.append(Tag(tagName))
        return tags
    
    def processPost(self,post):
        post.thread.parser.parsePost(post)
    
    def processThread(self,thread):
        for post in thread.posts:
            self.processPost(post)

                

    def AddToArchive(self):
        if self.options.thread and self.options.chan and self.options.chanTag:
            if not self.options.board:
                self.options.board = 'b'
            entry = meta.Session.query(ArchiveList).filter(ArchiveList.tid==self.options.thread).filter(ArchiveList.url==self.options.chan).filter(ArchiveList.board==self.options.board).first()
            if entry:
                print "Thread is already in the list"
            else:
                entry = ArchiveList()
                entry.tid = self.options.thread
                entry.url = self.options.chan
                entry.chanTag = self.options.chanTag
                entry.board = self.options.board
                entry.tags = self.options.tags or ''
                entry.type = self.options.type or 'wakaba'
                entry.filters = self.options.filters or ''
                entry.timeDiff = self.options.timeDiff or 0
                entry.lastChanged = datetime.datetime.fromtimestamp(0)
                meta.Session.save(entry)
                meta.Session.commit()
        else:
            print "Bad parameters"
    def UpdateArchive(self):
        archiveList = meta.Session.query(ArchiveList).all()
        for entry in archiveList:
            thread = Thread(entry,self.parsers)
            state = thread.checkState()
            print "*** Thread %s HTTP %s" % (thread.directlink,state[0])
            if state[0] == 404:
                meta.Session.delete(entry)
                meta.Session.commit()
            elif state[0] == 200:
                self.processThread(thread)
                entry.lastChanged = state[1]
                meta.Session.commit()
