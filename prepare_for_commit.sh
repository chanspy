#!/bin/sh

for file in `find . -name '*.pyc' -o -name '.*' -type f`
do
    echo "Removing $file..."
    rm "$file"
done
