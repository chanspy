import dbus.service

INTERFACE = 'py.chans.dbus'

class Thread(dbus.service.Object):

    def __init__(self, bus_name, obj_path, board, id):
        dbus.service.Object.__init__(self, bus_name, obj_path)
        self.bus_name = bus_name
        self.obj_path = obj_path
        self.board = board
        self.id = id
        self.posts = []

    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='aa{ss}')
    def get_posts_dicts(self):
        posts_dicts = []
        for post in self.posts:
            posts_dicts.append(post.post_dict)
        return posts_dicts
