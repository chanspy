import dbus.service

INTERFACE = 'py.chans.dbus'

class Chan(dbus.service.Object):
    """Chan class. Containing boards."""

    def __init__(self, bus_name, obj_path, name, base_uri, boards):
        dbus.service.Object.__init__(self, bus_name, obj_path)
        self.name = name
        self.base_uri = base_uri
        self.boards = boards

    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='as')
    def get_boards(self):
        """Get chan's boards names: ['b', 'i', 's']. Can be used by /pool/chan/board."""

        boards = []
        for board in self.boards:
            boards.append(board.name)
        return boards
