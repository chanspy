import dbus.service

from board import Board
from plugins.wakaba_thread import WakabaThread

import urllib
import re

INTERFACE = 'py.chans.dbus'

class MetaBoard(Board):
    """Wakaba board class."""

    def __init__(self, bus_name, obj_path, name, base_uri):
        Board.__init__(self, bus_name, obj_path, name, base_uri)

    #TODO: _normal_ parsing needs
    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='')
    def refresh(self):
        """Parsing board threads."""

        def get_threads_ids():
            data = urllib.urlopen(self.board_uri).read()
            threads = re.findall('<hr />.*?<a name="([0-9]+)"', data)
            return threads

        threads_ids = get_threads_ids()
        for thread_id in threads_ids:
            if not thread_id in self.get_threads():
                self.threads.append( WakabaThread(bus_name=self.bus_name, obj_path=self.obj_path +'/'+ thread_id, board=self, id=thread_id) )
