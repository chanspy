import dbus.service

from my_thread import Thread
from post import Post

import urllib
import re

INTERFACE = 'py.chans.dbus'

class WakabaThread(Thread):

    def __init__(self, bus_name, obj_path, board, id):
        Thread.__init__(self, bus_name, obj_path, board, id)

    #TODO: _normal_ parsing needs
    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='')
    def refresh(self):
        def get_posts_dicts():
            def parse_msg_table(post_data):
                post = {}
                post['id'] = re.findall('<a name="([0-9]+)">', post_data)[0]
                post['date'] = ''
                post['name'] = ''
                post['title'] = ''
                post['is_sage'] = ''
                img_src = re.findall('<a target="_blank" href="(.*?)">', post_data)
                if img_src:
                    post['img_src'] = img_src[0]
                else:
                    post['img_src'] = ''
                post['text'] = re.findall('<blockquote>(.*?)</blockquote>', post_data)[0]
                return post

            def get_msg_tables(data):
                return re.findall('<table><tbody><tr><td class="doubledash">(.*?)</td></tr></tbody></table>', data)

            data = urllib.urlopen('%s/%s/res/%s.html' %(self.board.base_uri, self.board.name, self.id)).read()

            first_post = re.findall('(<div id="thread-.*?</blockquote>)', data)[0]
            posts = [parse_msg_table(first_post)]
            for msg_table in get_msg_tables(data):
                posts.append(parse_msg_table(msg_table))
            return posts

        self.posts = []
        posts_dicts = get_posts_dicts()
        for post_dict in posts_dicts:
            self.posts.append( Post(bus_name=self.bus_name, obj_path=self.obj_path +'/'+ post_dict['id'], post=post_dict) )
