import dbus.service

INTERFACE = 'py.chans.dbus'

class ChanPool(dbus.service.Object):
    """ChanPool -- class that containing all chans."""

    def __init__(self, bus_name, obj_path, chans):
        dbus.service.Object.__init__(self, bus_name, obj_path)
        self.chans = chans

    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='as')
    def get_chans(self):
        """Returning array of chan names. Then it can be used by /pool/chan_name."""

        chans = []
        for chan in self.chans:
            chans.append(chan.name)
        return chans
