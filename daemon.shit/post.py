import dbus.service

INTERFACE = 'py.chans.dbus'

class Post(dbus.service.Object):
    """Post class."""

    def __init__(self, bus_name, obj_path, post):
        dbus.service.Object.__init__(self, bus_name, obj_path)
        self.post_dict = post

    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='aa{ss}')
    def get_post(self):
        return post_dict
