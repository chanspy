import dbus
import dbus.service

INTERFACE = 'py.chans.dbus'

class Board(dbus.service.Object):
    """Board class. Replacing by board plugins."""

    def __init__(self, bus_name, obj_path, name, base_uri):
        dbus.service.Object.__init__(self, bus_name, obj_path)
        self.bus_name = bus_name
        self.obj_path = obj_path
        self.name = name
        self.threads = []
        self.board_uri = '%s/%s/' %(base_uri, self.name)

    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='')
    def refresh(self):
        """Parsing board threads."""

        pass

    @dbus.service.method(dbus_interface=INTERFACE, in_signature='', out_signature='as')
    def get_threads(self):
        """Get threads: ['43244', '43245', '43246']."""

        threads = []
        for thread in self.threads:
            threads.append(thread.id)
        return threads
