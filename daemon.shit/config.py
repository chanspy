import sys
import yaml

def read_config(path):
    """Read yaml config."""

    options = {}
    try:
        file = open(path)
    except:
        sys.stderr.write('Can\'t find config file at %s/%s. Default settings will be load.\n' %(sys.path[0], path))
    else:
        options = yaml.load(file)

    return options
