import gobject
import dbus
import dbus.service
import dbus.mainloop.glib

import logging
import os
import imp

from chanpool import ChanPool
from chan import Chan

INTERFACE = 'py.chans.dbus'
OBJ_PATH = '/pool'

mainloop = gobject.MainLoop()

#----------------------------------------------
def start(logfile, opt_chans):
    """Loading plugins and run dbus mainloop."""

    # settings in config file?
    logging.basicConfig(level=logging.DEBUG, filename=logfile, format='[%(asctime)s] [%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    boards = load_plugins('plugins')

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    session_bus = dbus.SessionBus()
    tmp = dbus.service.BusName(INTERFACE, session_bus)

    chans = []
    for chan in opt_chans:                                         # [{'2ch': ...}, {'iichan': ...}]
        chan_name, settings = chan.popitem()

        myboards = []
        for board_dict in settings['boards']:                      # [{'wakaba': ['b', 's']}, {'kareha': ['beta/v', 'beta/a']}]
            board_type, boards_list = board_dict.popitem()

            if boards.has_key(board_type):
                for board in boards_list:                          # ['b', 's']
                    myboards.append( boards[board_type](session_bus, OBJ_PATH +'/'+ chan_name +'/'+ board, name=board, base_uri=settings['base_uri']) )

        chans.append( Chan(session_bus, OBJ_PATH +'/'+ chan_name, name=chan_name, base_uri=settings['base_uri'], boards=myboards) )

    ChanPool(session_bus, OBJ_PATH, chans)
    mainloop.run()

#----------------------------------------------
def load_plugins(path):
    """Load boards engines."""

    boards = {}

    for plugin in os.listdir(path):
        if plugin.startswith('plugin_') and plugin.endswith('.py'):
            plugin = plugin[:-3]
            board_type = plugin[7:]
            file, pathname, description = imp.find_module(path + '/' + plugin)

            #try:
            metaboard = imp.load_module(plugin, file, pathname, description).MetaBoard
            #except:
            #    logging.error('PLUGIN: can\'t load %s' %board_type)
            #else:
            boards[board_type] = metaboard
            logging.info('PLUGIN: %s loaded' %board_type)

    return boards

#----------------------------------------------
def stop():
    """Stopping services."""

    mainloop.quit()
