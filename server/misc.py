def to_bytes(string, encoding='utf-8'):
    if type(string) is unicode:
        string = string.encode(encoding)
    return string
