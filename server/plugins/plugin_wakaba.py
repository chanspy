import datetime
from BeautifulSoup import BeautifulSoup

import re
re_digits = re.compile('[0-9]*')

from base_plugin import BasePlugin

class Plugin(BasePlugin):
    """Wakaba plugin."""

    def get_threads_list(self, chan_name, board_name, chan_settings):
        data = self.get_uri('%s/%s/' %(chan_settings['base_uri'], board_name))
        threads = []
        if data:
            data = data.replace('<hr />', '<table id="mythread" class="mythread">')
            soup = BeautifulSoup(data)
            print('### Start findAll ### %s' %datetime.datetime.now())
            # tooooo slow ~0.8s
            threads_data = soup.findAll('table', {'id': 'mythread', 'class': 'mythread'})
            print('### End findAll   ### %s' %datetime.datetime.now())
            for i in range(1, len(threads_data) - 1):
                threads.append(self.parse_thread(threads_data[i]))
            threads.append(self.parse_thread(threads_data[2]))
        return threads

    def parse_thread(self, soup):
        thread_dict = { 'id':      ''
                      , 'date':    ''
                      , 'img_uri': ''
                      , 'title':   ''
                      , 'author':  ''
                      , 'message': ''
                      }

        thread_dict['id'] = dict(soup.find('a', {'name': re_digits}).attrs)['name']    # id
        label = soup.find('label').contents
        thread_dict['date'] = label[len(label) - 1]                                    # date
        img_uri = soup.find('a', {'target': '_blank'})
        if img_uri:
            thread_dict['img_uri'] = dict(img_uri.attrs)['href']                       # img_uri
        title = soup.find('span', {'class': 'filetitle'}).contents                     # title
        if title:
            thread_dict['title'] = title[0]
        author = soup.find('span', {'class': 'postername'}).contents
        if author:
            thread_dict['author'] = author[0]                                          # author
        message = soup.find('blockquote').p
        if message:
            thread_dict['message'] = message.contents[0]                               # message

        return thread_dict
