import urllib

class BasePlugin():
    '''Base plugin class. Replacing by users plugins.'''

    def get_threads_list(self, chan_name, board_name, chan_settings):
        return []

    #---------------------------------------------#
    def get_uri(self, uri):
        try:
            u = urllib.urlopen(uri)
            s = u.read()
            u.close()
        except:
            return None
        else:
            return s
