# -*- coding: utf-8 -*-

# Based on webserver.py by Jon Berg (turtlemeat.com)

import sys
import os
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import mimetypes

from html_generation import *
import misc

# some constants
htdocs_dir = u'htdocs'
server = None
settings = None
plugins = None

#---------------------------------------------#
class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        data = None
        ### actions starts with «/?»
        if self.path.startswith('/?'):
            action = self.path[2:]

            if action == 'settings':
                data = ('Унью!',)
            elif action == 'chans':
                chans_list = get_chans_list()
                data = (get_chans_html(chans_list),)
            else:
                self.send_error(501, 'Bad action: %s' %action)
        else:
            path = '%s%s' %(htdocs_dir, self.path)
            if os.path.isdir(path): path += '/index.html'

            ### physical file
            if os.path.exists(path):
                f = open(path)
                data = (f.read(), self.get_content_type(path))
                f.close()
            else:
                print self.path
                args = filter(None, self.path.split('/'))
                print args

                ### /chan/
                if len(args) == 1:
                    boards_list = get_boards_list(args[0])
                    if boards_list:
                        data = (get_boards_html(boards_list),)
                    else:
                        self.send_error(501, u'Can\'t find boards on %s' %args[0])
                ### /chan/board/
                elif len(args) == 2:
                    plugin = get_plugin(args[0], args[1])
                    if plugin:
                        threads_list = plugin.get_threads_list(args[0], args[1], get_chan_settings(args[0]))
                        data = (get_threads_html(threads_list, args[0], args[1]),)
                    else:
                        self.send_error(501, u'Can\'t find plugin for %s/%s/' %(args[0], args[1]))
                else:
                    self.send_error(404, u'File not found: %s' %path)

        if data:
            if len(data) == 1: data = (data[0], 'text/html')

            self.send_response(200)
            self.send_header(u'Content-type', data[1])
            self.end_headers()
            self.wfile.write(misc.to_bytes(data[0]))

    def get_content_type(self, filename):
        return mimetypes.guess_type(filename)[0] or 'application/octet-stream'
     
#---------------------------------------------#
def start_server(_settings, _plugins):
    global server, settings, plugins
    settings = _settings
    plugins = _plugins

    server = HTTPServer(('', settings['daemon']['port']), MyHandler)
    server.serve_forever()

#---------------------------------------------#
def stop_server():
    global server
    server.socket.close()

###############################################
def get_chans_list():
    chans_list = []
    for chan_item in settings['chans']:
        chan_name = chan_item.items()[0][0]
        chans_list.append((chan_name, u'/%s/' %chan_name))

    return chans_list

def get_boards_list(_chan_name):
    boards_list = []
    for chan_item in settings['chans']:
        chan_name, chan_settings = chan_item.items()[0]
        if _chan_name == chan_name:
            for board_item in chan_settings['boards']:
                board_name = board_item.items()[0][0]
                boards_list.append((board_name, u'/%s/%s/' %(chan_name, board_name)))

    return boards_list

def get_plugin(_chan_name, _board_name):
    for chan_item in settings['chans']:
        chan_name, chan_settings = chan_item.items()[0]
        if _chan_name == chan_name:
            for board_item in chan_settings['boards']:
                board_name, board_settings = board_item.items()[0]
                if _board_name == board_name:
                    plugin_name = board_settings['type']
                    if plugins.has_key(plugin_name):
                        return plugins[plugin_name]

def get_chan_settings(_chan_name):
    for chan_item in settings['chans']:
        chan_name, chan_settings = chan_item.items()[0]
        if _chan_name == chan_name:
            return chan_settings
