import os
import imp

# some constants
plugins_dir = 'plugins'

def load_plugins():
    '''Load plugins.'''

    plugins = {}

    for plugin in os.listdir(plugins_dir):
        if plugin.startswith('plugin_') and plugin.endswith('.py'):
            plugin = plugin[:-3]
            plugin_name = plugin[7:]
            file, pathname, description = imp.find_module('%s/%s' %(plugins_dir, plugin))

            #try:
            plugin_object = imp.load_module(plugin, file, pathname, description).Plugin()
            #except:
            #    logging.error('PLUGIN: can\'t load %s' %board_type)
            #else:
            plugins[plugin_name] = plugin_object
            #logging.info('PLUGIN: %s loaded' %board_type)

    return plugins
