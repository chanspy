# -*- coding: utf-8 -*-

import sys
import shutil
import os
from BeautifulSoup import BeautifulSoup

os.chdir(sys.path[0])
# some constants
htdocs_dir = u'htdocs'
chans_dir  = ['chans', os.path.join(htdocs_dir, u'chans')]
template   = os.path.join(htdocs_dir, u'list_template.html')
chans_file = os.path.join(chans_dir[1], u'index.html')

#---------------------------------------------#
def chans_generate(data, chans_list):
    '''Generate chans list.'''

    soup = BeautifulSoup(data)

    body = u'\n'
    for (chan_name, uri) in chans_list:
        # TODO: do something with this shit
        body += u'<div><a href="%s" target="BOARDS">%s</a></div>\n' %(uri, chan_name)

    soup.body.replaceWith(u'<body>%s</body>' %body)
    return unicode(soup)[:-1]

#---------------------------------------------#
def boards_generate(data, boards_list):
    '''Generate boards list.'''

    soup = BeautifulSoup(data)

    body = u'\n'
    for (board_name, uri) in boards_list:
        # TODO: do something with this shit
        body += u'<div><a href="%s" target="THREADS">%s</a></div>\n' %(uri, board_name)

    soup.body.replaceWith(u'<body>%s</body>' %body)
    return unicode(soup)[:-1]

#---------------------------------------------#
def generate(chans):
    '''Generate static html files.'''

    try:
        f = open(template, 'r')
        template_data = f.read()
        f.close()
        
        shutil.rmtree(chans_dir[1], 'ignore_errors')
        os.mkdir(chans_dir[1])

        chans_list = []
        for chan_item in chans:
            chan_name, chan_settings = chan_item.items()[0]
            chans_list.append((chan_name, u'/%s/%s/index.html' %(chans_dir[0], chan_name)))

            os.mkdir(os.path.join(chans_dir[1], chan_name))
            boards_list = []
            for board_item in chan_settings['boards']:
                board_name, board_settings = board_item.items()[0]
                boards_list.append((board_name, u'%s/%s/' %(chan_settings['base_uri'], board_name)))

            boards_html = boards_generate(template_data, boards_list)
            f = open(os.path.join(chans_dir[1], chan_name, 'index.html'), 'w')
            f.write(boards_html)
            f.close()

        chans_html = chans_generate(template_data, chans_list)
        f = open(chans_file, 'w')
        f.write(chans_html)
        f.close()
    except:
        pass
