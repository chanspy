import os
import sys
import yaml

# some constants
config_path = os.path.join('configs', 'config.yaml')

def load_config():
    '''Read yaml config.'''

    try:
        file = open(config_path)
        config = file.read()
    except:
        sys.stderr.write('Can\'t find config file at %s.\n' %os.path.join(sys.path[0], config_path))
        sys.exit(1)
    else:
        settings = yaml.load(config)
        return settings
