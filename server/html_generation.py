# -*- coding: utf-8 -*-

import sys
import shutil
import os
from BeautifulSoup import BeautifulSoup

# some constants
htdocs_dir = 'htdocs'
template = os.path.join(htdocs_dir, 'list_template.html')
try:
    f = open(template, 'r')
    template_data = f.read()
    f.close()
except:
    template_data = ''

#---------------------------------------------#
def get_chans_html(chans_list):
    '''Generate chans html.'''

    soup = BeautifulSoup(template_data)

    body = u'\n'
    for (chan_name, uri) in chans_list:
        body += u'<div class="chans_list"><a href="%s" target="BOARDS">%s</a></div>\n' %(uri, chan_name)

    soup.body.replaceWith(u'<body><div class="border">\n%s\n</div></body>' %body)
    return unicode(soup)[:-1]

#---------------------------------------------#
def get_boards_html(boards_list):
    '''Generate boards html.'''

    soup = BeautifulSoup(template_data)

    body = u'\n'
    for (board_name, uri) in boards_list:
        body += u'<div class="boards_list"><a href="%s" target="THREADS">%s</a></div>\n' %(uri, board_name)

    soup.body.replaceWith(u'<body><div class="border">\n%s\n</div></body>' %body)
    return unicode(soup)[:-1]

#---------------------------------------------#
def get_threads_html(threads_list, chan_name, board_name):
    '''Generate threads html.'''

    soup = BeautifulSoup(template_data)

    body = u'<h2 class="caption">%s — %s</h1>\n' %(chan_name, board_name)
    for thread_dict in threads_list:
        body += u'<div class="threads_list"><a name="%s"> %s | %s </a></div>\n' %( thread_dict['id'], thread_dict['title'], thread_dict['date'])

    soup.body.replaceWith(u'<body>%s</body>' %body)
    return unicode(soup)[:-1]
