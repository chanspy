import dbus

INTERFACE = 'py.chans.dbus'
bus = dbus.SessionBus()

#----------------------------------------------
def get_chans():
    """Loading chans from /pool via get_chans()."""

    pool_object = bus.get_object(INTERFACE, '/pool')
    pool = dbus.Interface(pool_object, INTERFACE)

    return pool.get_chans()

#----------------------------------------------
def get_boards(chan):
    """Loading boards from /pool/chan via get_boards()."""

    chan_object = bus.get_object(INTERFACE, '/pool/'+chan)
    chan = dbus.Interface(chan_object, INTERFACE)

    return chan.get_boards()

#----------------------------------------------
def refresh_board(chan, board):
    """Parsing board threads."""

    board_object = bus.get_object(INTERFACE, '/pool/'+chan+'/'+board)
    board = dbus.Interface(board_object, INTERFACE)
    board.refresh()

#----------------------------------------------
def get_threads(chan, board):
    """Loading boards from /pool/chan/board via get_threads()."""

    board_object = bus.get_object(INTERFACE, '/pool/'+chan+'/'+board)
    board = dbus.Interface(board_object, INTERFACE)

    return board.get_threads()
