import pygtk
pygtk.require("2.0")
import gtk
import gtk.glade

import client

class GUI:
    """GUI class. Processed all signals and get data via dbus from daemon."""

    def __init__(self, path):
        """Set starting gui parametrs and load some data -- chans and boards."""

        self.wTree = gtk.glade.XML(path)
        self.wTree.signal_autoconnect({ 'on_main_destroy': gtk.main_quit
                                      , 'on_chans_view_cursor_changed': self.board_selected
                                      , 'on_chans_view_row_activated': self.board_activated
                                      })

        # toolbar
        toolbar = self.wTree.get_widget('toolbar')
        bSettings = gtk.ToolButton(label='settings')
        bSettings.show()
        bExit = gtk.ToolButton(label='exit')
        bExit.show()
        bExit.connect('clicked', gtk.main_quit)
        toolbar.insert(bSettings, 0)
        toolbar.insert(bExit, 1)

        # statusbar
        self.statusbar = self.wTree.get_widget('statusbar')
        self.statusbar.push(0, 'Stop.')

        # chans_view
        chans_view = self.wTree.get_widget('chans_view')
        chans_tree = gtk.TreeStore(str)
        tvcolumn = gtk.TreeViewColumn('Chans')
        chans_view.append_column(tvcolumn)
        cell = gtk.CellRendererText()
        tvcolumn.pack_start(cell, True)
        tvcolumn.add_attribute(cell, 'text', 0)
        chans_view.set_model(chans_tree)

        chans = client.get_chans()
        for chan in chans:
            parent = chans_tree.append(None, [chan])
            boards = client.get_boards(chan)
            for board in boards:
                chans_tree.append(parent, [board])

        # threads_view
        threads_view = self.wTree.get_widget('threads_view')
        self.threads_list = gtk.ListStore(str, str)
        tvcolumn1 = gtk.TreeViewColumn('id')
        tvcolumn2 = gtk.TreeViewColumn('name')
        threads_view.append_column(tvcolumn1)
        threads_view.append_column(tvcolumn2)
        tvcolumn1.pack_start(cell, True)
        tvcolumn2.pack_start(cell, True)
        tvcolumn1.add_attribute(cell, 'text', 0)
        tvcolumn2.add_attribute(cell, 'text', 0)
        threads_view.set_model(self.threads_list)

    def main(self):
        """Starting gtk main loop."""

        gtk.main()

    def board_selected(self, treeview):
        """Select board."""

        model, path = treeview.get_selection().get_selected_rows()
        self.board_activated(treeview, path[0], None, False)

    def board_activated(self, treeview, path, view_column, refresh=True):
        """Load threads list into threads_view then we clicked on board in chans_view."""

        if len(path) == 2:
            model = treeview.get_model()
            chan_pos, board_pos = path

            iter_chan = model.get_iter((chan_pos,))
            chan = model.get_value(iter_chan, 0)
            iter_board = model.get_iter(path)
            board = model.get_value(iter_board, 0)

            if refresh: client.refresh_board(chan, board)
            threads = client.get_threads(chan, board)

            self.threads_list.clear()
            for thread in threads:
                self.threads_list.append([thread, 'nyaa'])
